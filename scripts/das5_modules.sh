# Extend MODULEPATH, so that openmpi modulefiles can be found
if [[ ${MODULEPATH} != *"/cm/shared/modulefiles"* ]]; then
  export MODULEPATH=$MODULEPATH:/cm/shared/modulefiles
fi

module load gcc/8.3.0
module load fftw/3.3.8-gcc-8.3.0
module load casacore/3.3.0-gcc-8.3.0
module load openblas/0.3.6-gcc-8.3.0
module load hdf5/1.10.6-gcc-8.3.0
module load wcslib/5.18-gcc-6.3.0
module load boost/1.73-gcc-8.3.0
module load openmpi/gcc/64/4.0.2

module load everybeam/LATEST-gcc-8.3.0
module load idg/LATEST-gcc-8.3.0-CUDA
